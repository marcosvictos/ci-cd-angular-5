import { Component } from '@angular/core';
import {AppServiceService} from './app-service.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[AppServiceService]
})
export class AppComponent {
  title = 'app';
  users:any;
  constructor(public appService:AppServiceService){

  }

  ngOnInit(){
    this.getUsers();
  }

  getUsers(){
    this.appService.getUsers().subscribe((res:any)=>{
      console.log(res);
    });
  }
}
