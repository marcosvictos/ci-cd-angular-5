import { Injectable } from '@angular/core';
import {HttpClient , HttpHeaders} from '@angular/common/http';
@Injectable()
export class AppServiceService {

  constructor( private http: HttpClient) { }

  getUsers(){
      return this.http.get('https://jsonplaceholder.typicode.com/users')
  }

}
